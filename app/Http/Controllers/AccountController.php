<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;

class AccountController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function profile() {
        $user = Auth::user();
        return view('profile', ['user' => $user]);
    }

    public function updateProfile(Request $request) {
        $data = $request->all();
        $user_id = Auth::user()->id;
        $validator = Validator::make($data, [
            'email' => 'required|max:255|unique:users,email,'.$user_id,
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone_number' => 'required',
            'address' => 'required',
            'sex' => 'required',
            'birth_day' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('profile')
                        ->withErrors($validator)
                        ->withInput();
        }
        unset($data['_token']);
        $data['status'] = 1;
        User::where('id', $user_id)->update($data);
        return redirect()->route('profile')->with('success', 'Cập nhật hồ sơ thành công !');
    }
}
