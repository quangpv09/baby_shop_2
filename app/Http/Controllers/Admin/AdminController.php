<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    //

    public function __construct()
   {
       $this->middleware('auth');
   }

    public function index() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $number_order = Order::where('status', 1)->count();
        $number_user = User::count();
        $number_product = Product::sum('number');
        return view('AdminLTE.pages.home', ['number_order' => $number_order, 'number_user' => $number_user, 'number_product' => $number_product]);
    }
}
