<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    //
    public function index() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $posts = Post::join('users', 'users.id', '=', 'posts.author_id')
                ->select('posts.*', 'users.first_name', 'users.last_name')        
                ->paginate(8);
        return view('AdminLTE.pages.blog.index', ['posts' => $posts]);
    }

    public function formCreate() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        return view('AdminLTE.pages.blog.create');
    }

    public function create(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'introduct' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.blog.formCreate')
                        ->withErrors($validator)
                        ->withInput();
        }

        $imageName = time().'.'.$request->image->extension();
        $request->image->storeAs('public/images', $imageName);
        $filePath = '/images/'.$imageName;
        $data['image'] = $filePath;
        $data['slug'] = Str::slug($data['title'], '-');
        $data['author_id'] = Auth::id();
        Post::create($data);
        return redirect()->route('admin.blog.index')->with('success', 'Create post success !');
    }

    public function formEdit($id) {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $post = Post::find($id);
        return view('AdminLTE.pages.blog.edit', ['post' => $post]);
    }

    public function edit(Request $request, $id) {
        $data = $request->all();
        unset($data['_token']);
        $validator = Validator::make($data, [
            'title' => 'required|string|max:255',
            'introduct' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.blog.formEdit', ['id' => $id])
                        ->withErrors($validator)
                        ->withInput();
        }

        if(isset($data['image'])) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->storeAs('public/images', $imageName);
            $filePath = '/images/'.$imageName;
            $data['image'] = $filePath;
        }
        $data['slug'] = Str::slug($data['title'], '-');
         Post::where('id', $id)->update($data);
        return redirect()->route('admin.blog.index')->with('success', 'Update post success !');
    }
    public function delete(Request $request) {
        $id = $request->id;
        Post::where('id', $id)->delete();
        return response()->json([
            'error' => false,
        ]);
    }
}
