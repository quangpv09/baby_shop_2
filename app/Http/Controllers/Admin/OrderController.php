<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $status = 1;
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }
        $orders = Order::join('products', 'products.id', '=', 'order.product_id')
        ->join('users', 'users.id', '=', 'order.user_id')
        ->where('order.status', $status)
        ->select('order.*','order.id as order_id', 'order.status as order_status' , 'products.image', 'products.name', 'products.code', 'products.price', 'users.*')
        ->paginate(8);
        return view('AdminLTE.pages.order.index', ['orders' => $orders]);
    }
}
