<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request) {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $search = $request->search;
        if ($search) {
            $search = Str::slug($search, '-');
            $products = Product::where('slug', 'like', '%' . $search . '%')->paginate(3);
        } else {
            $products = Product::paginate(3);
        }
        foreach($products as $prod) {
            $prod->category = Category::where('id', $prod->category_id)->first()->name;
        }
        return view('AdminLTE.pages.product.index', ['products' => $products]);
    }

    public function formCreate() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $categories = Category::all();
        return view('AdminLTE.pages.product.create', ['categories' => $categories]);
    }

    public function create(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'price' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'number' => 'required',
            'description' => 'required',
            'color' => 'required|array',
            'size' => 'required|array'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.formCreate')
                        ->withErrors($validator)
                        ->withInput();
        }

        $imageName = time().'.'.$request->image->extension();
        $request->image->storeAs('public/images', $imageName);
        $filePath = '/images/'.$imageName;
        $data['image'] = $filePath;
        $data['slug'] = Str::slug($data['name'], '-');
        $data['type'] = $data['type'] == 1 ? true : false;
        $data['color'] = implode(',', $data['color']);
        $data['size'] = implode(',', $data['size']);
        Product::create($data);
        return redirect()->route('admin.product.index')->with('success', 'Create product success !');
    }

    public function formEdit($id) {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        $categories = Category::all();
        $product = Product::find($id);
        return view('AdminLTE.pages.product.edit', ['categories' => $categories, 'product' => $product]);
    }

    public function edit(Request $request, $id) {
        $data = $request->all();
        unset($data['_token']);
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'price' => 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'number' => 'required',
            'description' => 'required',
            'color' => 'required|array',
            'size' => 'required|array'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.formEdit')
                        ->withErrors($validator)
                        ->withInput();
        }

        if(isset($data['image'])) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->storeAs('public/images', $imageName);
            $filePath = '/images/'.$imageName;
            $data['image'] = $filePath;
        }
        $data['slug'] = Str::slug($data['name'], '-');
        $data['color'] = implode(',', $data['color']);
        $data['size'] = implode(',', $data['size']);
         Product::where('id', $id)->update($data);
        return redirect()->route('admin.product.index')->with('success', 'Update product success !');
    }
    
    public function delete(Request $request) {
        $id = $request->id;
        Product::where('id', $id)->delete();
        return response()->json([
            'error' => false,
        ]);
    }
}
