<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    //
    public function index() {
        $posts = Post::paginate(8);
        return view('blogs.list', ['posts' => $posts]);
    }

    public function detail($slug) {
        $post = Post::where('slug', $slug)->first();
        return view('blogs.detail', ['post' => $post]);
    }
}
