<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    //
    public function index($category) {
        $category_id = Category::where('slug', $category)->first()->id;
        $list_product = Product::where('category_id', $category_id)->where('number', '>', 0)->paginate(3);
        $arr_id = [];
        foreach($list_product as $product) {
            $cat = Category::find($product->category_id);
            $product->category = $cat->slug;
            array_push($arr_id, $product->id);
        }
        return view('categories.index', ['category' => $category, 'list_product' => $list_product, 'arr_id' => $arr_id]);
    }

    public function getAllProduct(Request $request) {
        $search = $request->search;
        if ($search) {
            $search = Str::slug($search, '-');
            $list_product = Product::where('slug', 'like', '%' . $search . '%')->where('number', '>', 0)->paginate(3);
        } else {
            $list_product = Product::where('number', '>', 0)->paginate(3);
        }
        $arr_id = [];
        foreach($list_product as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
            array_push($arr_id, $product->id);
        }
        return view('categories.list-product', ['list_product' => $list_product, 'arr_id' => $arr_id]);
    }

    public function getHotProduct() {
        $list_product = Product::where('type', 1)->where('number', '>', 0)->paginate(3);
        $arr_id = [];
        foreach($list_product as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
            array_push($arr_id, $product->id);
        }
        return view('categories.list-product', ['list_product' => $list_product, 'arr_id' => $arr_id]);
    }

    public function getNewProduct() {
        $list_product = Product::where('type', 0)->where('number', '>', 0)->paginate(3);
        $arr_id = [];
        foreach($list_product as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
            array_push($arr_id, $product->id);
        }
        return view('categories.list-product', ['list_product' => $list_product, 'arr_id' => $arr_id]);
    }

    public function getSortByProduct(Request $request) {
        $category = $request->category;
        $sort_by = $request->sortBy;
        $page = $request->page ? $request->page : 1;
        $arr_id = json_decode($request->arrId);
        $list_product = DB::table('products');
        if ($category) {
            $category_id = Category::where('slug', $category)->first()->id;
            $list_product = $list_product->where('category_id', $category_id);
        }
        if ($page) {
            $list_product = $list_product->whereIn('id', $arr_id);
        }
        switch($sort_by) {
            case 'price-ascending':
                $list_product = $list_product->orderBy('price', 'ASC');
            case 'price-descending':
                $list_product = $list_product->orderBy('price', 'DESC');
            case 'title-ascending';
                $list_product = $list_product->orderBy('name', 'ASC');   
            case 'title-descending';
                $list_product = $list_product->orderBy('name', 'DESC'); 
            case 'created-ascending';
                $list_product = $list_product->orderBy('created_at', 'ASC');   
            case 'created-descending';
                $list_product = $list_product->orderBy('created_at', 'DESC');
            default:
                $list_product = $list_product->orderBy('price', 'ASC');
        }
        $list_product = $list_product->where('number', '>', 0)->take(3)->get();
        foreach($list_product as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
        }
        $returnHTML = view('ajax.list-product', ['list_product' => $list_product])->render();
        return response()->json([
            'error' => false,
            'data' => $returnHTML
        ]);
    }
}
