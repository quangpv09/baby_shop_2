<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listHotProduct = Product::where('type', 1)->where('number', '>', 0)->limit(8)->orderBy('id', 'DESC')->get();
        foreach($listHotProduct as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
        }
        $listNewProduct = Product::where('type', 0)->where('number', '>', 0)->limit(8)->orderBy('id', 'DESC')->get();
        foreach($listNewProduct as $product) {
            $category = Category::find($product->category_id);
            $product->category = $category->slug;
        }
        return view('home', ['listHotProduct' => $listHotProduct, 'listNewProduct' => $listNewProduct]);
    }   
}
