<?php

namespace App\Http\Controllers;

use App\Mail\SendMailOrder;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCart() {
        $user_id = Auth::user()->id;
        $orders = Order::join('products', 'products.id', '=', 'order.product_id')
            ->where('order.user_id', $user_id)
            ->where('order.status', 0)
            ->select('order.*', 'products.image', 'products.name', 'products.code', 'products.price')
            ->get();
        $totalProduct = 0;
        $totalPrice = 0;
        foreach($orders as $order) {
            $totalProduct += $order->amount;
            $totalPrice += ($order->price * $order->amount);
        }
        return view('cart', ['orders' => $orders, 'totalProduct' => $totalProduct, 'totalPrice' => $totalPrice]);
    }

    public function addToCart(Request $request) {
        $data = $request->all();
        if (isset($data['buy_now']) && $data['buy_now']) {
            $user = Auth::user();
            if (!$user->status) {
                return redirect(route('profile'));
            }
        } 
        $test = Order::create([
            'user_id' => Auth::user()->id,
            'product_id' => $data['product_id'],
            'size' => $data['size'],
            'color' => $data['color'],
            'amount' => $data['amount'],
            'status' => 0
        ]);
        Product::where('id', $data['product_id'])->decrement('number', $data['amount']);
        if (isset($data['buy_now']) && $data['buy_now'])
        return redirect(route('checkout'));
        return redirect(route('cart'));
    }

    public function addItem(Request $request) {
        $productId = $request->productId;
        $user = Auth::user();
        if (!$user->status) {
            return redirect(route('profile'));
        }
        Order::create([
            'user_id' => Auth::user()->id,
            'product_id' => $productId,
            'size' => 'M',
            'color' => 'Xanh',
            'amount' => 1,
            'status' => 0
        ]);
        Product::where('id', $productId)->decrement('number', 1);
        return response()->json([
            'error' => false
        ]);
    }

    public function updateCart(Request $request) {
        $data = $request->all();
        Order::where('id', $data['orderId'])->update(['amount' => $data['amount']]);
        return response()->json([
            'error' => false
        ]);
    }

    public function delete(Request $request) {
        $order = Order::find($request->orderId);
        Product::where('id', $order->product_id)->increment('number', $order->amount);
        $order->delete();
        return response()->json([
            'error' => false
        ]);
    }

    public function getCheckout() {
        $user = Auth::user();
        if (!$user->status) {
            return redirect(route('profile'));
        }
        $orders = Order::join('products', 'products.id', '=', 'order.product_id')
            ->where('order.user_id', $user->id)
            ->where('order.status', 0)
            ->select('order.*', 'products.image', 'products.name', 'products.code', 'products.price')
            ->get();
        if(count($orders) == 0) {
            return redirect('cart');
        }
        $totalPrice = 0;
        foreach($orders as $order) {
            $totalPrice += ($order->price * $order->amount);
        }
        return view('checkout', ['user' => $user, 'orders' => $orders, 'totalPrice' => $totalPrice]);
    }

    public function checkout(Request $request) {
        $user_id = $request->userId;
        Order::where('user_id', $user_id)->update([
            'status' => 1
        ]);
        Mail::to(Auth::user()->email)->send(new SendMailOrder(''));
        return response()->json([
            'error' => false
        ]);
    }

    public function updateStatus(Request $request) {
        $id = $request->id;
        $status = $request->status;
        $order = Order::where('id', $id)->update([
            'status' => $status + 1
        ]);
        return response()->json([
            'error' => false
        ]);
    }

    public function cancelOrder(Request $request) {
        $id = $request->id;
        $order = Order::where('id', $id)->update([
            'status' => 5
        ]);
        return response()->json([
            'error' => false
        ]);
    }
}
