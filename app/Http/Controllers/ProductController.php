<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function detail($category, $product) {
        $category = Category::where('slug', $category)->first();
        if(!empty($category)) {
            $category_id = $category->id;
            $product = Product::where('slug', $product)->where('category_id', $category_id)->first();
            return view('products.detail', ['product' => $product]);
        }
    }
}
