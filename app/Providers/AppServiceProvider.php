<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $menus = Category::whereNull('parent_id')->get();
        foreach($menus as $menu) {
            $menu->sub_menu = Category::where('parent_id', $menu->id)->get();
        }
        view()->composer('*', function ($view) 
        {
            $number_cart = 0;
            if(Auth::check()) {
                $orders = Order::where('status', 0)->where('user_id', Auth::id())->get();
                foreach($orders as $order) {
                    $number_cart += $order->amount;
                }
            }
            $view->with('number_cart', $number_cart );    
        }); 
        view()->share('menus', $menus);
    }
}
