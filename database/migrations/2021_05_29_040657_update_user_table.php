<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->nullable()->default('/images/avatar-default.png');
            $table->string('phone_number')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->boolean('sex')->nullable()->default(null);
            $table->date('birth_day')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users');

        Schema::dropIfExists('users');
    }
}
