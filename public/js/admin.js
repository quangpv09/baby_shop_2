$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.submitDeleteCategory').on('click', function() {
        let id = $(this).data('id')
        $.ajax({
            url: '/admin/category/delete',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(result){
                if(!result.error) {
                    $('#deleteCategoryModal-'+id).modal('hide')
                    Swal.fire(
                        'Success!',
                        '',
                        'success'
                      )
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000)
                }
            }
        })
    })

    $('.submitDeleteUser').on('click', function() {
        let id = $(this).data('id')
        $.ajax({
            url: '/admin/user/delete',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(result){
                if(!result.error) {
                    $('#deleteUserModal-'+id).modal('hide')
                    Swal.fire(
                        'Success!',
                        '',
                        'success'
                      )
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000)
                }
            }
        })
    })

    ClassicEditor
    .create( document.querySelector( '#editor' ), {
        // ckfinder: {
        //     uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
        // }
    } )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );

    $('.submitDeleteProduct').on('click', function() {
        let id = $(this).data('id')
        $.ajax({
            url: '/admin/product/delete',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(result){
                if(!result.error) {
                    $('#deleteProductModal-'+id).modal('hide')
                    Swal.fire(
                        'Success!',
                        '',
                        'success'
                      )
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000)
                }
            }
        })
    })
    $('.submitDeletePost').on('click', function() {
        let id = $(this).data('id')
        $.ajax({
            url: '/admin/blog/delete',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(result){
                if(!result.error) {
                    $('#deletePostModal-'+id).modal('hide')
                    Swal.fire(
                        'Success!',
                        '',
                        'success'
                      )
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000)
                }
            }
        })
    })
})
$('.serach-product').on('click', function() {
    console.log('123')
    $('#searchProduct').submit()
})
$(".js-color-tags").select2({
    tags: true
  });
  $(".js-size-tags").select2({
    tags: true
  });
  $('.submitConfirmOrder').on('click', function() {
      let id = $(this).data('id');
      let status = $(this).data('status');
      $.ajax({
        url: '/order/updateStatus',
        type: 'post',
        dataType: 'json',
        data: {
            id: id,
            status: status
        },
        success: function(result){
            if(!result.error) {
                $('#confirmModal-'+id).modal('hide')
                Swal.fire(
                    'Success!',
                    '',
                    'success'
                  )
                setTimeout(function() {
                    window.location.href = window.location.origin + '/admin/order?status=' + (status + 1);
                }, 2000)
            }
        }
    })
  })

  $('.submitCancelOrder').on('click', function() {
    let id = $(this).data('id');
    $.ajax({
      url: '/order/cancelOrder',
      type: 'post',
      dataType: 'json',
      data: {
          id: id,
      },
      success: function(result){
          if(!result.error) {
              $('#cancelOrderModal-'+id).modal('hide')
              Swal.fire(
                  'Success!',
                  '',
                  'success'
                )
              setTimeout(function() {
                  window.location.href = window.location.origin + '/admin/order?status=5';
              }, 2000)
          }
      }
  })
})