/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $('.menu-category .list-item .item i').on('click', function () {
        let item = $(this).closest('.item');
        item.find('.sub-list-item').toggle();
    })
})

$('.sort-by').on('change', function() {
    let sortBy =  $(this).find(":selected").val();
    let category = $('#category').val();
    let page = $('#page').val();
    let arrId = $('#arrId').val();
    $.ajax({
        url: '/list-product/sort-by',
        type: 'get',
        dataType: 'json',
        data: {
            category: category,
            sortBy: sortBy,
            page: page,
            arrId: arrId
        },
        success: function(result){
            if(!result.error) {
                let productList = $('.product-list');
               productList.html(result.data);
               console.log(result.data);
            }
        }
    })
})

$('.submit-search').on('click', function() {
    $('.form-search').submit();
})
$('.cart-item__cell-quantity .sub').on('click', function() {
    let totalAmount = $('.total-price .txt-price .total').text();
    let totalPrice = $('.total-price .price').text();
    let cartItem = $(this).closest('.cart-item');
    let amount = cartItem.find('.amount').val();
    let price = cartItem.find('.cart-item__cell-unit-price span').text();
    let orderId = cartItem.find('.order-id').val();
    amount = parseInt(amount) - 1;
    totalAmount = parseInt(totalAmount) - 1;
    if(amount < 1) return
    $.ajax({
        url: '/order/update-cart',
        type: 'post',
        dataType: 'json',
        data: {
            orderId: orderId,
            amount: amount
        },
        success: function(result){
            
        }
    })
    cartItem.find('.amount').val(amount);
    cartItem.find('.cart-item__cell-total-price span').text(parseInt(price) * amount+'₫');
    $('.total-price .txt-price .total').text(totalAmount);
    $('.total-price .price').text(parseInt(totalPrice) - parseInt(price)+'₫');
    $('.cart-number').text(totalAmount);
})

$('.cart-item__cell-quantity .sum').on('click', function() {
    let totalAmount = $('.total-price .txt-price .total').text();
    let totalPrice = $('.total-price .price').text();
    let cartItem = $(this).closest('.cart-item');
    let amount = cartItem.find('.amount').val();
    let price = cartItem.find('.cart-item__cell-unit-price span').text();
    let orderId = cartItem.find('.order-id').val();
    amount = parseInt(amount) + 1;
    totalAmount = parseInt(totalAmount) + 1;
    $.ajax({
        url: '/order/update-cart',
        type: 'post',
        dataType: 'json',
        data: {
            orderId: orderId,
            amount: amount
        },
        success: function(result){
            
        }
    })
    cartItem.find('.amount').val(amount);
    cartItem.find('.cart-item__cell-total-price span').text(parseInt(price) * amount+'₫');
    $('.total-price .txt-price .total').text(totalAmount);
    $('.total-price .price').text(parseInt(totalPrice) + parseInt(price)+'₫');
    $('.cart-number').text(totalAmount);
})

$('.delete-order').on('click', function() {
    let cartItem = $(this).closest('.cart-page-shop-section__items');
    let orderId = cartItem.find('.order-id').val();
    $.ajax({
        url: '/order/delete',
        type: 'post',
        dataType: 'json',
        data: {
            orderId: orderId,
        },
        success: function(result){
            if(!result.error) {
                let totalAmount = $('.total-price .txt-price .total').text();
                let totalPrice = $('.total-price .price').text();
                let amount = cartItem.find('.amount').val();
                let price = cartItem.find('.cart-item__cell-unit-price span').text();
                $('.total-price .txt-price .total').text(totalAmount - amount);
                $('.total-price .price').text(parseInt(totalPrice) - (parseInt(price) * amount)+'₫');
                $('.cart-number').text(totalAmount - amount);
                cartItem.remove();
            }
        }
    })
})

$('.submitCheckout').on('click', function() {
    let userId = $(this).data('id');
    $.ajax({
        url: '/checkout',
        type: 'post',
        dataType: 'json',
        data: {
            userId: userId,
        },
        success: function(result){
            if(!result.error) {
                $('#checkoutModal').modal('hide')
                Swal.fire(
                    'Thành công!',
                    '',
                    'success'
                  )
                setTimeout(function() {
                    window.location.reload();
                }, 2000)
            }
        }
    })
})

$('#buy-now').on('click', function() {
    event.preventDefault();
    $('.checkBuyNow').val(true);
    $('#add-item-form').submit();
})

$('#add-to-cart').on('click', function() {
    event.preventDefault();
    let numberProduct = $('.number-product').val();
    let itemQuantity = $('.item-quantity').val();
    if (parseInt(numberProduct) < parseInt (itemQuantity)) {
        alert('Số lượng sản phẩm còn lại không đủ !')
    } else {
        $('#add-item-form').submit();
    }
})

$('.add-item').on('click', function() {
    let id = $(this).data('id')
    $.ajax({
        url: '/order/add-item',
        type: 'post',
        dataType: 'json',
        data: {
            productId: id,
        },
        success: function(result){ 
           if(!result.error) {
               let cartNumber = $('.cart-number').text();
               $('.cart-number').text(parseInt(cartNumber) + 1)
               $('#addItemSuccess').modal('show');
           }
        }
    })
})

$('.btn-addcart').on('click', function() {
    event.preventDefault();
    let id = $(this).data('id')
    let numerProduct = $(this).data('number-product')
    let itemQuantity = $('#previewProductModal-'+id).find('.item-quantity').val()
    if (parseInt(numerProduct) < parseInt(itemQuantity)) {
        alert('Số lượng sản phẩm còn lại không đủ !')
    } else {
        $('#previewProductModal-'+id).find('form').submit()
    }
})