@extends('AdminLTE.index')

@section('content_admin')
<!-- Content Header (Page header) -->
<style>
.nav-tab .list-item{
  width: 100%;
    margin-bottom: 12px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    overflow: hidden;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 1;
    background: #fff;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
}
.nav-tab .tab-item {
  cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    padding: 16px 0;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: rgba(0,0,0,.8);
    background: #fff;
    border-bottom: 2px solid rgba(0,0,0,.09);
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -webkit-flex: 1;
    -moz-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    overflow: hidden;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -moz-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -moz-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-transition: color .2s;
    transition: color .2s;
}
.nav-tab a:hover{
  color: #ee4d2d;
}
.nav-tab .active {
  border-color: #ee4d2d;
}
</style>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Order manage</h1>
          </div><!-- /.col -->
         
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <div class="nav-tab">
      <div class=row>
      <div class="col-md-12" style="padding-left: 23px; padding-right: 23px;">
        <div class="list-item">
          <a href="{{route('admin.order.index')}}?status=1" class="tab-item {{((isset($_GET['status']) && $_GET['status'] == 1) || (!isset($_GET['status']))) ? 'active' : ''}}">
            Chờ xác nhận
          </a>
          <a href="{{route('admin.order.index')}}?status=2" class="tab-item {{(isset($_GET['status']) && $_GET['status'] == 2) ? 'active' : ''}}">
            Chờ lấy hàng
          </a>
          <a href="{{route('admin.order.index')}}?status=3" class="tab-item {{(isset($_GET['status']) && $_GET['status'] == 3) ? 'active' : ''}}">
            Đang giao
          </a>
          <a href="{{route('admin.order.index')}}?status=4" class="tab-item {{(isset($_GET['status']) && $_GET['status'] == 4) ? 'active' : ''}}">
            Đã giao
          </a>
          <a href="{{route('admin.order.index')}}?status=5" class="tab-item {{(isset($_GET['status']) && $_GET['status'] == 5) ? 'active' : ''}}">
            Hủy
          </a>
        </div>
      </div>
      </div>
    </div>
    <!-- /.content-header -->
      <section class="content">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                  @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                  @endif
                  <table class="table" style="background: #fff">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Image</th>
                          <th scope="col">Name</th>
                          <th scope="col">Code</th>
                          <th scope="col">Color</th>
                          <th scope="col">Size</th>
                          <th scope="col">Price</th>
                          <th scope="col">User name</th>
                          <!-- <th scope="col">Email</th> -->
                          <th scope="col">Phone number</th>
                          <th scope="col">Address</th>
                          <th scope="col">Created at</th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($orders as $k => $order)
                            <tr>
                              <th scope="row">{{ $k + 1 }}</th>
                              <td><img style="width: 100px; height: 100px;" src="{{ url('storage'.$order->image)}}"></td>
                              <td>{{ $order->name}}</td>
                              <td>{{ $order->code}}</td>
                              <td>{{ $order->color}}</td>
                              <td>{{ $order->size}}</td>
                              <td>{{ $order->price}}</td>
                              <td>{{ $order->first_name}} {{ $order->last_name}}</td>
                              <!-- <td>{{ $order->email}}</td> -->
                              <td>{{ $order->phone_number}}</td>
                              <td>{{ $order->address}}</td>
                              <td>{{ $order->created_at}}</td>
                              @if(in_array($order->order_status, [1, 2, 3]))
                              <td><button type="button" class="btn btn-primary confirm-order" data-toggle="modal" data-target="#confirmModal-{{$order->order_id}}">Confirm</button></td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cancelOrderModal-{{$order->order_id}}">Cancel</button></td>
                              @endif
                            </tr>
                            <div class="modal fade" id="confirmModal-{{$order->order_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <h3 style="text-align: center;margin-top: 25px;margin-bottom: 25px;">Are you sure to confirm?</h3>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" data-status="{{$order->order_status}}" data-id="{{$order->order_id}}" class="btn btn-primary submitConfirmOrder">Confirm</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal fade" id="cancelOrderModal-{{$order->order_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <h3 style="text-align: center;margin-top: 25px;margin-bottom: 25px;">Are you sure to cancel?</h3>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" data-status="{{$order->order_status}}" data-id="{{$order->order_id}}" class="btn btn-primary submitCancelOrder">Cancel</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                      </tbody>
                    </table>
                    {{ $orders->links() }}
                  </div>
                </div>
            </div><!-- /.container-fluid -->
          </section>
@endsection