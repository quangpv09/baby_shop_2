@extends('AdminLTE.index')

@section('content_admin')
<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice__display {
    color: #333;
}
</style>
<div class="content-header">
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Create product</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert" style="margin-top: 15px;">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <form method="POST" action="{{ route('admin.product.create') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="categoryName">Name</label>
                        <input type="text" class="form-control" id="categoryName" name="name">
                    </div>
                    <div class="form-group">
                        <label for="categoryCode">Code</label>
                        <input type="text" class="form-control" id="categoryCode" name="code">
                    </div>
                    <div class="form-group">
                        <label for="categoryPrice">Price</label>
                        <input type="number" class="form-control" id="categoryPrice" name="price">
                    </div>
                    <div class="form-group">
                        <label for="categoryImage">Image</label>
                        <input type="file" onchange="readURL(this);" id="categoryImage" name="image">
                        <img id="blah" src="#" alt="your image" />
                    </div>
                    <div class="form-group">
                        <label for="categoryID">Category</label>
                        <select class="form-control" id="categoryID" name="category_id">
                            @foreach($categories as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Type</label>
                        <select class="form-control" name="type" id="exampleFormControlSelect1">
                            <option value="1">Hot</option>
                            <option value="0">New</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Number</label>
                        <input type="number" class="form-control" name="number">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Color</label>
                        <select class="form-control js-color-tags" name="color[]" multiple="multiple">
                            <option>Vàng</option>
                            <option>Hồng</option>
                            <option>Xanh</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Size</label>
                        <select class="form-control js-size-tags" name="size[]" multiple="multiple">
                            <option>S</option>
                            <option>M</option>
                            <option>L</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="categoryDescription">Description</label>
                        <textarea id="editor" name="description">
                            <!-- <p>Here goes the initial content of the editor.</p> -->
                        </textarea>
                    </div>
            
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>
<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection