@extends('AdminLTE.index')

@section('content_admin')
<div class="content-header">
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Create user</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert" style="margin-top: 15px;">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <form method="POST" action="{{ route('admin.user.create') }}">
                    @csrf
                    <div class="form-group">
                        <label for="categoryName">First name</label>
                        <input type="text" class="form-control" id="categoryName" name="first_name">
                    </div>
                    <div class="form-group">
                        <label for="categoryName">Last name</label>
                        <input type="text" class="form-control" id="categoryName" name="last_name">
                    </div>
                    <div class="form-group">
                        <label for="categoryName">Email</label>
                        <input type="email" class="form-control" id="categoryName" name="email">
                    </div>
                    <div class="form-group">
                        <label for="categoryName">Password</label>
                        <input type="password" class="form-control" id="categoryName" name="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Role</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="role">
                            <option value="user">User</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>
@endsection