@foreach($list_product as $prod)
    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
        <div class="product-block product-resize fixheight" style="height: 335px;">
            <div class="product-img image-resize view view-third" style="height: 260px;">
                <a href="{{route('product.detail', ['category' => $prod->category, 'product' => $prod->slug])}}" title="Xe trượt HDL">
                    <img class="first-image  has-img" alt=" Xe trượt HDL " src="{{url('storage'.$prod->image)}}">
                </a>    
                <div class="actionss">
                    <div class="btn-cart-products">
                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814358)">
                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="view-details">
                        <a href="/products/xe-truot-hdl" class="view-detail">
                            <span><i class="fa fa-clone"> </i></span>
                        </a>
                    </div>
                    <div class="btn-quickview-products">
                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-truot-hdl"><i class="fa fa-eye"></i></a>
                    </div>
                </div>

            </div>
            <div class="product-detail clearfix">
                <!-- sử dụng pull-left -->
                <h3 class="pro-name"><a href="/products/xe-truot-hdl" title="{{$prod->name}}">{{$prod->name}} </a></h3>
                <div class="pro-prices">
                    <p class="pro-price">{{number_format($prod->price, 0, '', ',')}}₫</p>
                    <p class="pro-price-del text-left"></p>
                </div>
            </div>
        </div>

    </div>
@endforeach