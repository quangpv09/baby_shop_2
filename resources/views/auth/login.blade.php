@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-create" id="layout-page">
	<span class="header-contact header-page clearfix">
		<h1>Đăng nhập</h1>
	</span>

            <div id="customer-login">
                <div id="login" class="userbox">
                    <div class="accounttype">
                        <h2 class="title"></h2>
                    </div>
                    <form accept-charset="UTF-8" method="POST" action="{{ route('login') }}" id="customer_login">
                        @csrf
                        @foreach ($errors->all() as $error)
                            <span class="invalid-feedback d-block" role="alert">{{ $error }}</li>
                        @endforeach
                        <div class="clearfix large_form">
                            <label for="customer_email" class="icon-field"><i class="fa fa-envelope-o" aria-hidden="true"></i></label>
                            <input required="" type="email" value="" name="email" id="customer_email" placeholder="Email" class="text">
                        </div>


                        <div class="clearfix large_form">
                            <label for="customer_password" class="icon-field"><i class="fa fa-lock" aria-hidden="true"></i></label>
                            <input required="" type="password" value="" name="password" id="customer_password" placeholder="Mật khẩu" class="text" size="16">
                        </div>


                        <div class="action_bottom">
                            <input class="btn btn-signin" type="submit" value="Đăng nhập">
                        </div>
                        <div class="req_pass">
                            <a href="#" onclick="showRecoverPasswordForm();return false;">Quên mật khẩu?</a>

                            hoặc <a href="{{ route('register') }}" title="Đăng ký">Đăng ký</a>
                        </div>


                </div>

                <div id="recover-password" style="display:none;" class="userbox">
                    <div class="accounttype">
                        <h2>Phục hồi mật khẩu</h2>
                    </div>

                    <form accept-charset="UTF-8" action="/account/recover" method="post">
                        <input name="form_type" type="hidden" value="recover_customer_password">
                        <input name="utf8" type="hidden" value="✓">



                        <label for="email" class="icon-field"><i class="icon-login icon-envelope "></i></label>
                        <input type="email" value="" size="30" name="email" placeholder="Email" id="recover-email" class="text">

                        <div class="action_bottom">
                            <input class="btn" type="submit" value="Gửi">
                        </div>
                        <div class="req_pass">
                            <a href="#" onclick="hideRecoverPasswordForm();return false;">Hủy</a>
                        </div>

                </div>


            </div>
        </div>
    </div>
</div>
@endsection
