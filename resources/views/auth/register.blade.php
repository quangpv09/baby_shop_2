@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div id="layout-page" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-create">
	<span class="header-contact header-page clearfix">
		<h1>Tạo tài khoản</h1>
	</span>
            <div class="userbox">
                <form accept-charset="UTF-8" id="create_customer" method="POST" action="{{ route('register') }}">
                    @csrf
                    @foreach ($errors->all() as $error)
                        <span class="invalid-feedback d-block" role="alert">{{ $error }}</li>
                    @endforeach
                    <div id="last_name" class="clearfix large_form">
                        <label for="last_name" class="label icon-field"><i class="fa fa-user-o" aria-hidden="true"></i></label>
                        <input required="" type="text" value="" name="last_name" placeholder="Họ" id="last_name" class="text" size="30">
                    </div>
                    <div id="first_name" class="clearfix large_form">
                        <label for="first_name" class="label icon-field"><i class="fa fa-user-o" aria-hidden="true"></i></label>
                        <input required="" type="text" value="" name="first_name" placeholder="Tên" id="first_name" class="text" size="30">
                    </div>
                    <div id="email" class="clearfix large_form">
                        <label for="email" class="label icon-field"><i class="fa fa-envelope-o" aria-hidden="true"></i></label>
                        <input required="" type="email" value="" placeholder="Email" name="email" id="email" class="text" size="30">
                    </div>
                    <div id="password" class="clearfix large_form">
                        <label for="password" class="label icon-field"><i class="fa fa-lock" aria-hidden="true"></i></label>
                        <input required="" type="password" value="" placeholder="Mật khẩu" name="password" id="password" class="password text" size="30">
                    </div>
                    <div class="action_bottom">
                        <input class="btn" type="submit" value="Đăng ký">
                    </div>
                    <div class="req_pass">
                        <a class="come-back" href="https://default-baby.myharavan.com"><i class="fa fa-long-arrow-left"></i> Quay lại trang chủ</a>
                    </div>

            </div>

        </div>
    </div>
</div>
@endsection
