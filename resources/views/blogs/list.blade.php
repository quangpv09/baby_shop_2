@extends('layouts.app')

@section('content')
    <div class="blog">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>


                                <li><a href="/collections" target="_self">Danh mục</a></li>



                                <li class="active"><span>Tất cả sản phẩm</span></li>



                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('sidebar.sidebar')
                    </div>
                    <div class="col-md-9">
                        <div class="articles clearfix" id="layout-page">
                            <div class="col-md-12  col-sm-12 col-xs-12">
                                <h1>Tin tức</h1>
                            </div>
                            @foreach($posts as $post)
                            <div class="news-content row">

                                <div class="col-md-5 col-xs-12 col-sm-12 img-article">
                                    <div class="art-img">
                                        <img src="{{url('storage'.$post->image)}}" alt="">
                                    </div>
                                </div>


                                <div class=" col-md-7 col-sm-12  col-xs-12">
                                    <!-- Begin: Nội dung bài viết -->
                                    <h2 class="title-article"><a href="{{route('blog.detail', ['slug' => $post->slug])}}">{{$post->title}}</a></h2>
                                    <div class="body-content">
                                        <ul class="info-more">
                                            <li><i class="fa fa-calendar-o"></i><time pubdate="" datetime="{{$post->created_at}}">{{$post->created_at}}</time></li>
                                            <li><i class="fa fa-file-text-o"></i><a href="#"> Tin tức	</a> </li>
                                            {{$post->introduct}}
                                        </ul>
                                        <p></p>
                                    </div>
                                    <!-- End: Nội dung bài viết -->
                                    <a class="readmore btn-rb clear-fix" href="{{route('blog.detail', ['slug' => $post->slug])}}" role="button">Xem tiếp <span class="fa fa-angle-double-right"></span></a>
                                </div>


                            </div>
                            <hr class="line-blog">
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

