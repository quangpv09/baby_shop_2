@extends('layouts.app')

@section('content')
    <div class="contact">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>
                                <li class="active"><span>Giỏ hàng</span></li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12" id="layout-page">

                    <span class="header-contact header-page clearfix">
                        <h1 style="text-align: left">Giỏ hàng</h1>
                    </span>
                        @if(count($orders))
                        <div class="content-contact content-page">
                            <div class="cart-page-product-header">
                            <div class="cart-page-product-header__product" style="padding-left: 25px;">Sản phẩm</div>
                            <div class="cart-page-product-header__unit-price">Đơn giá</div>
                            <div class="cart-page-product-header__quantity">Số lượng</div>
                            <div class="cart-page-product-header__total-price">Số tiền</div>
                            <div class="cart-page-product-header__action">Thao tác</div></div>
                        </div>
                        @foreach($orders as $order)
                            <div class="cart-page-shop-section__items">
                                <div class="bundle-group">
                                    <div class="cart-item">
                                    <div class="cart-item__content">
                                        <input hidden type="text" class="order-id" value="{{$order->id}}">
                                        <div class="cart-item__cell-overview">
                                            <div class="cart-item__cell-overview--line">
                                                <a href="#"><img style="width: 5rem; height: 5rem;" src="{{url('storage'.$order->image)}}"></a>
                                                <div class="cart-item-overview__product-name-wrapper">
                                                    <a class="cart-item-overview__name" href="#">{{$order->name}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-item__cell-variation">
                                            <div>Phân loại hàng:</div>
                                            <div>{{$order->code}}, {{$order->color}}, {{$order->size}}</div>
                                        </div>
                                        <div class="cart-item__cell-unit-price">
                                            <span>{{$order->price}}₫</span>
                                        </div>
                                        <div class="cart-item__cell-quantity"><div class="_16mL_A shopee-input-quantity"><button class="_2KdYzP sub"><svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg></button><input class="_2KdYzP iRO3yj amount" type="text" value="{{$order->amount}}"><button class="_2KdYzP sum"><svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg></button></div></div>
                                        <div class="cart-item__cell-total-price">
                                            <span>{{$order->price * $order->amount}}₫</span>
                                        </div>
                                        <div class="cart-item__cell-actions cart-item__cell-actions--multiple">
                                            <button class="cart-item__action delete-order">Xóa</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="cart-footer cart-page-footer">
                            <div class="total-price">
                                <span class="txt-price">Tổng thanh toán (<span class="total">{{$totalProduct}}</span> Sản phẩm):</span>
                                <span class="price">{{$totalPrice}}₫</span>
                            </div>
                            <div class="btn-checkout">
                                <a style="color: #fff; text-decoration: none;" href="{{route('checkout')}}">
                                    <button class="shopee-button-solid shopee-button-solid--primary ">
                                        <span class="cart-page-footer__checkout-text">Mua hàng</span>
                                    </button>
                                </a>
                            </div>
                        </div>
                        @else
                            <p class="text-center">Không có sản phẩm nào trong giỏ hàng!</p>
                            <p class="text-center"><a style="text-decoration: none; color: #333333" href="{{route('category.allProduct')}}">
				                <i class="fa fa-reply"></i> Tiếp tục mua hàng</a>
			                </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
