@extends('layouts.app')

@section('content')
    <div class="category">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>


                                <li><a href="{{route('category.index', ['category' => $category])}}" target="_self">{{$category}}</a></li>



                                <li class="active"><span>Tất cả sản phẩm</span></li>



                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('sidebar.sidebar')
                    </div>
                    <div class="col-md-9">
                            <div class="sort-collection">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h1>
                                            Tất cả sản phẩm
                                        </h1>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="browse-tags">
                                            <span>Sắp xếp theo:</span>
                                            <span class="custom-dropdown custom-dropdown--white">
                                                <input type="text" hidden id="category" value="{{$category}}">
                                                <input type="text" hidden id="page" value="{{app('request')->input('page')}}">
                                                <input type="text" hidden id="arrId" value="{{json_encode($arr_id)}}">
										<select class="sort-by custom-dropdown__select custom-dropdown__select--white">

											<option value="price-ascending">Giá: Tăng dần</option>
											<option value="price-descending">Giá: Giảm dần</option>
											<option value="title-ascending">Tên: A-Z</option>
											<option value="title-descending">Tên: Z-A</option>
											<option value="created-ascending">Cũ nhất</option>
											<option value="created-descending">Mới nhất</option>
											<option value="best-selling">Bán chạy nhất</option>
										</select>
									</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 content-product-list">
                                <div class="row product-list">
                                    @foreach($list_product as $prod)
                                        <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
                                            <div class="product-block product-resize fixheight" style="height: 335px;">
                                                <div class="product-img image-resize view view-third" style="height: 260px;">
                                                    <a href="{{route('product.detail', ['category' => $category, 'product' => $prod->slug])}}" title="Xe trượt HDL">
                                                        <img class="first-image  has-img" alt=" Xe trượt HDL " src="{{url('storage'.$prod->image)}}">
                                                    </a>    
                                                    <div class="actionss">
                                                        <div class="btn-cart-products">
                                                            <a href="javascript:void(0);" data-id="{{$prod->id}}" class="add-item">
                                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                        <div class="view-details">
                                                            <a href="{{route('product.detail', ['category' => $prod->category, 'product' => $prod->slug])}}" class="view-detail">
                                                                <span><i class="fa fa-clone"> </i></span>
                                                            </a>
                                                        </div>
                                                        <div class="btn-quickview-products">
                                                            <a href="javascript:void(0);" class="quickview"data-toggle="modal" data-target="#previewProductModal-{{$prod->id}}"><i class="fa fa-eye"></i></a>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="product-detail clearfix">
                                                    <!-- sử dụng pull-left -->
                                                    <h3 class="pro-name"><a href="/products/xe-truot-hdl" title="{{$prod->name}}">{{$prod->name}} </a></h3>
                                                    <div class="pro-prices">
                                                        <p class="pro-price">{{number_format($prod->price, 0, '', ',')}}₫</p>
                                                        <p class="pro-price-del text-left"></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal fade" id="previewProductModal-{{$prod->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style="background-color: #0096d9;">
                                            <h4 style="font-weight: bold; color: #ffffff;text-transform: uppercase; font-size: 13px;" class="modal-title" id="exampleModalLabel">{{$prod->name}}</h5>
                                            <button type="button" style="color: #fff; opacity: unset;" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('order.addToCart')}}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <img style="max-height: 300px; max-width: 100%;" src={{url('storage'.$prod->image)}}>
                                                    </div>
                                                    <div class="col-md-7">
                                                    <input hidden type="text" name="product_id" value="{{$prod->id}}">
                                                        <div class="product-price" style="width: 300px; padding: 10px 0">
                                                        {{number_format($prod->price, 0, '', ',')}}
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 30px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Kích thước</label>
                                                            <select class="form-control" name="size">
                                                                @foreach(explode(',', $prod->size) as $size)
                                                                    <option value="{{$size}}">{{$size}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 17px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Màu sắc</label>
                                                            <select class="form-control" name="color">
                                                                @foreach(explode(',', $prod->color) as $color)
                                                                    <option value="{{$color}}">{{$color}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Số lượng</label>
                                                            <input type="number" value="1" class="form-control item-quantity" name="amount" style="width: 60px;">
                                                        </div>
                                                    <div>
                                                    <button type="submit" data-id="{{$prod->id}}" data-number-product="{{$prod->number}}" class="btn-detail  btn-color-add btn-min-width btn-mgt btn-addcart" style="display: block; float: left;">Thêm vào giỏ</button>
                                                        <div class="qv-readmore" style="float: left;padding: 22px 10px;">
                                                            <span> hoặc </span><a class="read-more p-url" href="{{route('product.detail', ['category' => $category, 'product' => $prod->slug])}}" role="button">Xem chi tiết</a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    @endforeach
                               </div>
                            </div>
                             {{$list_product->appends(request()->input())->links()}} 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addItemSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Thêm sản phẩm thành công !</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
</div>
@endsection