@extends('layouts.app')

@section('content')
    <div class="contact">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>
                                <li class="active"><span>Thanh toán</span></li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="_1WlhIE">
                <div class="_1iurwE">
                    <div class="_1G9Cv7"></div>
                    <div class="_1gkIPw">
                        <div class="_1TKMuK">
                            <div class="_20Qrq_">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                Địa chỉ nhận hàng
                            </div>
                        </div>
                        <div class="Jgz_oc">
                            <div class="_2Pe7Hh">
                                <div class="_3E850P">{{$user->first_name}} {{$user->last_name}} (+84) {{$user->phone_number}}</div>
                                <div class="_2F7jaW">{{$user->address}}</div>
                                <div class="_2LiNia">Mặc định</div>
                            </div>
                            <div class="_1xGx71"><a href="{{route('profile')}}">Thay đổi</a></div>
                        </div>
                    </div>
                </div>
                <div class="_1p19xl">
                <div class="ktZs2X">
                    <div class="_1_W4l_">
                        <div class="_3GZI6L mpT3lP" style="height: 74px">
                            <div class="_3NO6B5">Sản phẩm</div>
                        </div>
                        <div class="_3GZI6L _2vvZhb"></div>
                        <div class="_3GZI6L">Đơn giá</div>
                        <div class="_3GZI6L">Số lượng</div>
                        <div class="_3GZI6L _17w1DK">Thành tiền</div>
                    </div>
                </div>
                <div>
                    @foreach($orders as $order)
                    <div class="jNDkp2">
                        <div>
                        <div class="QjLA16">
                            <div class="_1oOvbg">
                                <div class="_3HkBPE _3vbMPD">
                                    <div class="_1ASQkt _2rJzUE">
                                        <img class="_1Qtf1H" src="{{url('storage'.$order->image)}}" width="40" height="40">
                                        <span class="_11r44J"><span class="_3F5vLQ">{{$order->name}}</span></span>
                                    </div>
                                    <div class="_1ASQkt Aw_HtH">
                                        <span class="_3y8KEH">Loại: {{$order->code}}, {{$order->color}}, {{$order->size}}</span>
                                    </div>
                                    <div class="_1ASQkt">{{$order->price}}₫</div>
                                    <div class="_1ASQkt">{{$order->amount}}</div>
                                    <div class="_1ASQkt _2z5WqO">{{$order->price * $order->amount}}₫</div>
                                </div>
                            </div>
                        </div>
                        <div class="_1MFx1Y"><div class="_3519w5">Tổng số tiền ({{$order->amount}} sản phẩm):</div><div class="-c5EIK">{{$order->price * $order->amount}}₫</div></div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="PC1-mc" style="margin-top: 30px">
                    <div class="_1i3wS2 _1X3--o RihLPS">Tổng tiền hàng</div>
                    <div class="_1i3wS2 lsNObX RihLPS">{{$totalPrice}}₫</div>
                    <!-- <div class="_1i3wS2 _1X3--o RRMXiE">Phí vận chuyển</div>
                    <div class="_1i3wS2 lsNObX RRMXiE">₫36.780</div> -->
                    <div class="_1i3wS2 _1X3--o _3QL9JJ">Tổng thanh toán:</div>
                    <div class="_1i3wS2 _20-5lO lsNObX _3QL9JJ">{{$totalPrice}}₫</div>
                    <div class="_3Q9F5R _1Q3ggw"><div class="_2xyh_G"></div>
                    <button data-toggle="modal" data-target="#checkoutModal" style="background-color: #0096d9;color: #fff;" class="checkout stardust-button stardust-button--primary stardust-button--large _1qSlAe _2a74ER">Đặt hàng</button></div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body" style="padding: 50px;">
                <h3>Bạn có muốn đặt hàng không?</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="button" data-id="{{$user->id}}" class="btn btn-primary submitCheckout">Đặt hàng</button>
            </div>
            </div>
        </div>
    </div>
@endsection
