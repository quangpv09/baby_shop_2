@extends('layouts.app')

@section('content')
    <div class="contact">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>
                                <li class="active"><span>Liên hệ</span></li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12" id="layout-page">

		<span class="header-contact header-page clearfix">
			<h1 style="text-align: left">Liên hệ</h1>
		</span>

                        <div class="content-contact content-page">

                            <p class="text-center">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.310729061172!2d106.6539626784669!3d10.787496261978522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ecbb6275cc3%3A0x634b1cfb9f2ed0be!2zNTYgVsOibiBDw7RpLCBwaMaw4budbmcgNywgVMOibiBCw6xuaCwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1446015505979" width="100%" height="350" frameborder="0" style="border:0; pointer-events: none;" allowfullscreen=""></iframe>
                            </p>


                            <div class="row">
                                <div class="col-md-7 col-sm-12 col-xs-12 contactFormWrapper" id="col-left ">
                                    <h3>Viết nhận xét</h3>
                                    <hr class="line-left">
                                    <p>
                                        Nếu bạn có thắc mắc gì, có thể gửi yêu cầu cho chúng tôi, và chúng tôi sẽ liên lạc lại với bạn sớm nhất có thể .
                                    </p>
                                    <form accept-charset="UTF-8" action="/contact" class="contact-form" method="post">
                                        <input name="form_type" type="hidden" value="contact">
                                        <input name="utf8" type="hidden" value="✓">




                                        <div class="form-group">
                                            <label for="contactFormName" class="sr-only">Tên</label>
                                            <input required="" type="text" id="contactFormName" class="form-control input-lg" name="contact[name]" placeholder="Tên của bạn" autocapitalize="words" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="contactFormEmail" class="sr-only">Email</label>
                                            <input required="" type="email" name="contact[email]" placeholder="Email của bạn" id="contactFormEmail" class="form-control input-lg" autocorrect="off" autocapitalize="off" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="contactFormMessage" class="sr-only">Nội dung</label>
                                            <textarea required="" rows="6" name="contact[body]" class="form-control" placeholder="Viết bình luận" id="contactFormMessage"></textarea>
                                        </div>

                                        <input type="submit" class="btn btn-primary btn-lg btn-rb" value="Gửi liên hệ">


                                        <input id="7ac8a44eb04c46988f2bf6f7c7232926" name="g-recaptcha-response" type="hidden" value="03AGdBq24KqXZsTQWLz4b0sL5P1eIxHPGgckFrccl9y3Yzb4PBvAzNYiIx_h67mxG4H2svwyi16umJAlRMR-W4XcHpf-9vkBrwVFiSxtIAWLwItSfWfW952dQj2MBEnLMxsCmA1Zv-ePSkrmcnxPvjmHa3eIIgt5TCOsijbXfF54AVZY64HmlhI3-8tFgHv-fBgKJkjR4SfcMpQTNexriCQkYfE4py4bKAmLKcwoqHd2UXCb8SrDV3IVdttyixN2_VJXtYAzICANf22TzT2TOpLM8Z-Cf_GxDg401oPoIFsFgz2GRkYx2mNCwyPKuU06k7KSNrv0f1SMWJRKdFNNgKbccf_Zrph3VraNrZflNYlygtgaIJrqQfh4UCpAPD6DO-IbymbCFLqXikDjlW7ZTDoMI9Ym8BbAjPDWDk61qKNDU-cBDoLJIeb10uycdXDavVBpN8kD5I9ro9Z-4Wfa3MHv5uDfZWfloEmg"><script src="https://www.google.com/recaptcha/api.js?render=6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-"></script><script>grecaptcha.ready(function() {grecaptcha.execute('6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-', {action: 'submit'}).then(function(token) {document.getElementById('7ac8a44eb04c46988f2bf6f7c7232926').value = token;});});</script></form>

                                </div>

                                <div class="col-md-5 sm-12 col-xs-12 " id="col-right">
                                    <h3>Chúng tôi ở đây</h3>
                                    <hr class="line-right">
                                    <h3 class="name-company">Fashion Style</h3>
                                    <p>	Giải pháp bán hàng toàn diện từ website đến cửa hàng	</p>
                                    <ul class="info-address">
                                        <li>
                                            <i style="width: 16px" class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span>56 Vân côi, Phường 7, Quận Tân Bình, Tp. Hồ Chí Minh</span>
                                        </li>
                                        <li>
                                            <i style="width: 16px" class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>hi@haravan.com</span>
                                        </li>

                                        <li>
                                            <i style="width: 16px" class="fa fa-phone" aria-hidden="true"></i>
                                            <span>1900.636.099</span>
                                        </li>

                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
