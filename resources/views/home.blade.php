@extends('layouts.app')

@section('content')
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_1.jpg?v=21" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_2.jpg?v=21" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_3.jpg?v=21" alt="Third slide">
                    </div>
                </div>
            </div>
    <div class="hone-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="list-product">
                        <div class="list-product-highlight">
                            <aside class="styled_header  use_icon ">
                                <h2>What hot</h2>

                                <h3>Sản phẩm nổi bật</h3>
                                <span class="icon"><img src="https://hstatic.net/333/1000150333/1000203368/icon_featured.png?v=21" alt=""></span>

                            </aside>
                            <div class="row">
                                @foreach($listHotProduct as $hotProduct) 
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="{{route('product.detail', ['category' => $hotProduct->category, 'product' => $hotProduct->slug])}}">
                                                <img src="{{url('storage'.$hotProduct->image)}}">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart({{$hotProduct->id}})">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="{{route('product.detail', ['category' => $hotProduct->category, 'product' => $hotProduct->slug])}}" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-toggle="modal" data-target="#previewProductModal-{{$hotProduct->id}}"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">{{$hotProduct->name}}</a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>{{number_format($hotProduct->price, 0, '', ',')}}₫</p>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                                <div class="modal fade" id="previewProductModal-{{$hotProduct->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style="background-color: #0096d9;">
                                            <h4 style="font-weight: bold; color: #ffffff;text-transform: uppercase; font-size: 13px;" class="modal-title" id="exampleModalLabel">{{$hotProduct->name}}</h5>
                                            <button type="button" style="color: #fff; opacity: unset;" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('order.addToCart')}}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <img style="max-height: 300px; max-width: 100%;" src={{url('storage'.$hotProduct->image)}}>
                                                    </div>
                                                    <div class="col-md-7">
                                                    <input hidden type="text" name="product_id" value="{{$hotProduct->id}}">
                                                        <div class="product-price" style="width: 300px; padding: 10px 0">
                                                        {{number_format($hotProduct->price, 0, '', ',')}}
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 30px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Kích thước</label>
                                                            <select class="form-control" name="size">
                                                                @foreach(explode(',', $hotProduct->size) as $size)
                                                                    <option value="{{$size}}">{{$size}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 17px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Màu sắc</label>
                                                            <select class="form-control" name="color">
                                                                @foreach(explode(',', $hotProduct->color) as $color)
                                                                    <option value="{{$color}}">{{$color}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Số lượng</label>
                                                            <input type="number" value="1" class="form-control item-quantity" name="amount" style="width: 60px;">
                                                        </div>
                                                    <div>
                                                    <button type="submit" data-id="{{$hotProduct->id}}" data-number-product="{{$hotProduct->number}}" class="btn-detail  btn-color-add btn-min-width btn-mgt btn-addcart" style="display: block; float: left;">Thêm vào giỏ</button>
                                                        <div class="qv-readmore" style="float: left;padding: 22px 10px;">
                                                            <span> hoặc </span><a class="read-more p-url" href="{{route('product.detail', ['category' => $hotProduct->category, 'product' => $hotProduct->slug])}}" role="button">Xem chi tiết</a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-center center">
                                    <a class="btn btn-readmore" href="{{route('category.hotProduct')}}" role="button">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="animation fade-in home-banner-1">
                                    <aside class="banner-home-1">
                                        <div class="divcontent"><span class="ad_banner_1">Miễn phí vận chuyển<strong class="ad_banner_2">Với tất cả đơn hàng trên 500k</strong></span>
                                            <span class="ad_banner_desc">Miễn phí 2 ngày vận chuyển với đơn hàng trên 500k trừ trực tiếp khi thanh toán</span>
                                        </div>
                                        <div class="divstyle"></div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                        <div class="list-product-new">
                            <aside class="styled_header  use_icon ">


                                <h3>Sản phẩm mới</h3>
                                <span class="icon"><img src="//hstatic.net/333/1000150333/1000203368/icon_sale.png?v=21" alt="Newest trends"></span>

                            </aside>
                            <div class="row">
                            @foreach($listNewProduct as $newProduct)
                            <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="{{url('storage'.$newProduct->image)}}">         
                                            </a>
                                             <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart($newProduct->id)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="{{route('product.detail', ['category' => $newProduct->category, 'product' => $newProduct->slug])}}" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-toggle="modal" data-target="#previewProductModal-{{$newProduct->id}}"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">{{$newProduct->name}}</a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>{{number_format($newProduct->price, 0, '', ',')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="previewProductModal-{{$newProduct->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style="background-color: #0096d9;">
                                            <h4 style="font-weight: bold; color: #ffffff;text-transform: uppercase; font-size: 13px;" class="modal-title" id="exampleModalLabel">{{$newProduct->name}}</h5>
                                            <button type="button" style="color: #fff; opacity: unset;" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('order.addToCart')}}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <img style="max-height: 300px; max-width: 100%;" src={{url('storage'.$newProduct->image)}}>
                                                    </div>
                                                    <div class="col-md-7">
                                                    <input hidden type="text" name="product_id" value="{{$newProduct->id}}">
                                                        <div class="product-price" style="width: 300px; padding: 10px 0">
                                                        {{number_format($newpProduct->price, 0, '', ',')}}
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 30px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Kích thước</label>
                                                            <select class="form-control" name="size">
                                                                @foreach(explode(',', $newpProduct->size) as $size)
                                                                    <option value="{{$size}}">{{$size}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="width: 200px; margin-top: 17px;">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Màu sắc</label>
                                                            <select class="form-control" name="color">
                                                                @foreach(explode(',', $newpProduct->color) as $color)
                                                                    <option value="{{$color}}">{{$color}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color: #333333; font-size: 13px; font-weight: bold;">Số lượng</label>
                                                            <input type="number" value="1" class="form-control item-quantity" name="amount" style="width: 60px;">
                                                        </div>
                                                    <div>
                                                    <button type="submit" data-id="{{$newpProduct->id}}" data-number-product="{{$newpProduct->number}}" class="btn-detail  btn-color-add btn-min-width btn-mgt btn-addcart" style="display: block; float: left;">Thêm vào giỏ</button>
                                                        <div class="qv-readmore" style="float: left;padding: 22px 10px;">
                                                            <span> hoặc </span><a class="read-more p-url" href="{{route('product.detail', ['category' => $newProduct->category, 'product' => $newProduct->slug])}}" role="button">Xem chi tiết</a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                            @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-center center">
                                    <a class="btn btn-readmore" href="{{route('category.newProduct')}}" role="button">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="banner-bottom">
                            <div class="row">

                                <div class="col-xs-12 col-sm-6 home-category-item-2">
                                    <div class="block-home-category">
                                        <a href="collections/hot-products">
                                            <img class="b-lazy b-loaded" src="https://hstatic.net/333/1000150333/1000203368/block_home_category1.jpg?v=21" alt="Thời trang nữ">
                                        </a>
                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-6 home-category-item-3">
                                    <div class="block-home-category">
                                        <a href="collections/hot-products">
                                            <img class="b-lazy b-loaded" src="https://hstatic.net/333/1000150333/1000203368/block_home_category2.jpg?v=21" alt="Thời trang nam">
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addItemSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Thêm sản phẩm thành công !</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
</div>
<script>
        function add_item_show_modalCart(id) {
            $.ajax({
                url: '/order/add-item',
                type: 'post',
                dataType: 'json',
                data: {
                    productId: id,
                },
                success: function(result){ 
                   if(!result.error) {
                       let cartNumber = $('.cart-number').text();
                       $('.cart-number').text(parseInt(cartNumber) + 1)
                       $('#addItemSuccess').modal('show');
                   }
                }
            })
        }
</script>
@endsection
        