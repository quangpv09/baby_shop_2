@extends('layouts.app')

@section('content')
    <div class="contact">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>
                                <li class="active"><span>Hồ sơ của tôi</span></li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container _1QwuCJ" style="width: 1200px">
                <div class="userpage-sidebar">
                    <div class="user-page-brief">
                        <a class="user-page-brief__avatar">
                            <div class="shopee-avatar">
                                <img class="shopee-avatar__img" src="{{url('storage'.$user->avatar)}}">
                            </div>
                        </a>
                        <div class="user-page-brief__right">
                            <div class="user-page-brief__username">{{$user->first_name}} {{$user->last_name}}</div>
                            <div>
                                <a class="user-page-brief__edit" href="{{route('profile')}}">
                                    <svg width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg" style="margin-right: 4px;">
                                        <path d="M8.54 0L6.987 1.56l3.46 3.48L12 3.48M0 8.52l.073 3.428L3.46 12l6.21-6.18-3.46-3.48" fill="#9B9B9B" fill-rule="evenodd"></path>
                                    </svg>Sửa hồ sơ
                                </a>
                            </div> 
                        </div>
                    </div>
                    <div class="userpage-sidebar-menu">
                        <div class="userpage-sidebar-menu-entry">
                            <div class="userpage-sidebar-menu-entry__icon">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            </div>
                            <div class="userpage-sidebar-menu-entry__text"><a href="{{route('profile')}}">Tài khoản của tôi</a></div>
                        </div>
                        <!-- <div class="userpage-sidebar-menu-entry">
                            <div class="userpage-sidebar-menu-entry__icon">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                            </div>
                            <div class="userpage-sidebar-menu-entry__text">Đơn mua</div>
                        </div> -->
                    </div>
                </div>
                <div class="_3D9BVC">
                    <div class="h4QDlo">
                        <div class="_2HWwrr">
                            <div class="_8C1CP">
                                <h1 class="_1OYAxY">Hồ sơ của tôi</h1>
                                <div class="_2MZ69L">Quản lý thông tin hồ sơ để bảo mật tài khoản</div>
                            </div>
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            <div class="goiz2O _3u2vYq">
                                <div class="pJout2">
                                    <form action="{{route('updateProfile')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Họ</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <div class="input-with-validator">
                                                            <input name="first_name" type="text" placeholder="" maxlength="255" value="{{$user->first_name}}">
                                                        </div>
                                                        @error('first_name')
                                                            <div class="msg-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Tên</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <div class="input-with-validator">
                                                            <input name="last_name" type="text" placeholder="" maxlength="255" value="{{$user->last_name}}">
                                                        </div>
                                                        @error('last_name')
                                                            <div class="msg-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Email</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <div class="input-with-validator">
                                                            <input name="email" type="email" placeholder="" maxlength="255" value="{{$user->email}}">
                                                        </div>
                                                        @error('email')
                                                            <div class="msg-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Số điện thoại</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <div class="input-with-validator">
                                                            <input name="phone_number" type="text" placeholder="" maxlength="255" value="{{$user->phone_number ? $user->phone_number : ''}}">
                                                        </div>
                                                        @error('phone_number')
                                                            <div class="msg-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Địa chỉ</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <textarea name="address" class="form-control" rows="3">{{$user->address ? $user->address : ''}}</textarea>
                                                    </div>
                                                    @error('address')
                                                        <div class="msg-error">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Giới Tính</label>
                                                </div>
                                                <div class="_2_JugQ" style="margin-left: 25px">
                                                    <div class="input-with-validator-wrapper" style="display: flex">
                                                        <div style="width: 65px">
                                                            <input class="form-check-input" type="radio" name="sex" value="1" {{$user->sex === 1 ? 'checked' : ''}}>
                                                            <label class="form-check-label" for="exampleRadios1">
                                                                Nam
                                                            </label>
                                                        </div>
                                                        <div style="width: 65px">
                                                            <input class="form-check-input" type="radio" name="sex" value="0" {{$user->sex === 0 ? 'checked' : ''}}>
                                                            <label class="form-check-label" for="exampleRadios1">
                                                                Nữ
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                    @error('sex')
                                                        <div class="msg-error">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_3BlbUs">
                                            <div class="_1iNZU3">
                                                <div class="_2PfA-y">
                                                    <label>Ngày Sinh</label>
                                                </div>
                                                <div class="_2_JugQ">
                                                    <div class="input-with-validator-wrapper">
                                                        <div class="input-with-validator">
                                                            <input name="birth_day" type="date" placeholder="" maxlength="255" value="{{$user->birth_day ? $user->birth_day : ''}}">
                                                        </div>
                                                        @error('birth_day')
                                                            <div class="msg-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_31PFen">
                                            <button type="submit" class="btn btn-primary" aria-disabled="false">Lưu</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="_1aIEbS">
                                    <div class="X1SONv">
                                        <div class="_1FzaUZ">
                                            <div class="TgSfgo" style="background-image: url(&quot;{{url('storage'.$user->avatar)}}&quot;);"></div>
                                        </div>
                                        <input class="_2xS5eV" type="file" accept=".jpg,.jpeg,.png">
                                        <button type="button" class="btn btn-light">Chọn ảnh</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
