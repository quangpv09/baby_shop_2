<div class="menu-category">
    <div class="title">
        <h3>Danh mục</h3>
    </div>
    <div class="list-item">
        <ul>
            @foreach($menus as $menu)
                <li class="item">
                    <a href="{{route('category.index', ['category' => $menu->slug])}}">{{$menu->name}}</a>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="sub-list-item">
                        @if($menu->sub_menu)
                        @foreach($menu->sub_menu as $sub_menu)
                            <li class="sub-item">
                                <a href="{{route('category.index', ['category' => $sub_menu->slug])}}">{{$sub_menu->name}}</a>
                            </li>
                        @endforeach
                       @endif
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>
