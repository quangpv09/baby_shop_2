<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
// Admin

// Route::get('/admin', 'Admin\AdminController@index');
Route::prefix('admin')->group(function () {
    Route::get('/dashboard', 'Admin\AdminController@index')->name('admin');
    Route::get('/category', 'Admin\CategoryController@index')->name('admin.category.index');
    Route::get('/category/create', 'Admin\CategoryController@formCreate')->name('admin.category.formCreate');
    Route::post('/category/create', 'Admin\CategoryController@create')->name('admin.category.create');
    Route::get('/category/edit/{id}', 'Admin\CategoryController@formEdit')->name('admin.category.formEdit');
    Route::post('/category/edit/{id}', 'Admin\CategoryController@edit')->name('admin.category.edit');
    Route::post('/category/delete', 'Admin\CategoryController@delete')->name('admin.category.delete');

    Route::get('/user', 'Admin\UserController@index')->name('admin.user.index');
    Route::get('/user/create', 'Admin\UserController@formCreate')->name('admin.user.formCreate');
    Route::post('/user/create', 'Admin\UserController@create')->name('admin.user.create');
    Route::get('/user/edit/{id}', 'Admin\UserController@formEdit')->name('admin.user.formEdit');
    Route::post('/user/edit/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
    Route::post('/user/delete', 'Admin\UserController@delete')->name('admin.user.delete');

    Route::get('/product', 'Admin\ProductController@index')->name('admin.product.index');
    Route::get('/product/create', 'Admin\ProductController@formCreate')->name('admin.product.formCreate');
    Route::post('/product/create', 'Admin\ProductController@create')->name('admin.product.create');
    Route::get('/product/edit/{id}', 'Admin\ProductController@formEdit')->name('admin.product.formEdit');
    Route::post('/product/edit/{id}', 'Admin\ProductController@edit')->name('admin.product.edit');
    Route::post('/product/delete', 'Admin\ProductController@delete')->name('admin.product.delete');

    Route::get('/order', 'Admin\OrderController@index')->name('admin.order.index');

    Route::get('/blog', 'Admin\BlogController@index')->name('admin.blog.index');
    Route::get('/blog/create', 'Admin\BlogController@formCreate')->name('admin.blog.formCreate');
    Route::post('/blog/create', 'Admin\BlogController@create')->name('admin.blog.create');
    Route::get('/blog/edit/{id}', 'Admin\BlogController@formEdit')->name('admin.blog.formEdit');
    Route::post('/blog/edit/{id}', 'Admin\BlogController@edit')->name('admin.blog.edit');
    Route::post('/blog/delete', 'Admin\BlogController@delete')->name('admin.blog.delete');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/category/all-product', 'CategoryController@getAllProduct')->name('category.allProduct');
Route::get('/{category}/list-product', 'CategoryController@index')->name('category.index');
Route::get('/list-product/sort-by', 'CategoryController@getSortByProduct');
Route::get('/category/hot-product', 'CategoryController@getHotProduct')->name('category.hotProduct');
Route::get('/category/new-product', 'CategoryController@getNewProduct')->name('category.newProduct');
Route::post('/order/add-to-cart', 'OrderController@addToCart')->name('order.addToCart');
Route::post('/order/add-item', 'OrderController@addItem')->name('order.addItem');
Route::post('/order/buy-now', 'OrderController@buyNow')->name('order.buyNow');
Route::get('/cart', 'OrderController@getCart')->name('cart');
Route::get('/account/profile', 'AccountController@profile')->name('profile');
Route::post('/account/updateProfile', 'AccountController@updateProfile')->name('updateProfile');
Route::post('/order/update-cart', 'OrderController@updateCart')->name('order.updateCart');
Route::post('/order/updateStatus', 'OrderController@updateStatus')->name('order.updateStatus');
Route::post('/order/cancelOrder', 'OrderController@cancelOrder')->name('order.cancelOrder');
Route::post('/order/delete', 'OrderController@delete')->name('order.delete');
Route::get('/checkout', 'OrderController@getCheckout')->name('getCheckout');
Route::post('/checkout', 'OrderController@checkout')->name('checkout');
Route::get('contact', 'ContactController@index')->name('contact');
Route::get('blog', 'BlogController@index')->name('blog');
Route::get('blog/{slug}', 'BlogController@detail')->name('blog.detail');
Route::get('/{category}/{product}', 'ProductController@detail')->name('product.detail');
